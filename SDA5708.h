const uint8_t DISPLAY_FONT[] PROGMEM = {
    // 0
        0b00001110,
        0b00010001,
        0b00010011,
        0b00010101,
        0b00011001,
        0b00010001,
        0b00001110,
    // 1
        0b00000100,
        0b00001100,
        0b00000100,
        0b00000100,
        0b00000100,
        0b00000100,
        0b00001110,
    // 2
        0b00001110,
        0b00010001,
        0b00000001,
        0b00000110,
        0b00001000,
        0b00010000,
        0b00011111,
    // 3
        0b00001110,
        0b00010001,
        0b00000001,
        0b00000110,
        0b00000001,
        0b00010001,
        0b00001110,
    // 4
        0b00000010,
        0b00000110,
        0b00001010,
        0b00010010,
        0b00011111,
        0b00000010,
        0b00000010,
    // 5
        0b00011111,
        0b00010000,
        0b00011110,
        0b00000001,
        0b00000001,
        0b00010001,
        0b00001110,
    // 6
        0b00000110,
        0b00001000,
        0b00010000,
        0b00011110,
        0b00010001,
        0b00010001,
        0b00001110,
    // 7
        0b00011111,
        0b00000001,
        0b00000010,
        0b00000100,
        0b00001000,
        0b00001000,
        0b00001000,
    // 8
        0b00001110,
        0b00010001,
        0b00010001,
        0b00001110,
        0b00010001,
        0b00010001,
        0b00001110,
    // 9
        0b00001110,
        0b00010001,
        0b00010001,
        0b00001111,
        0b00000001,
        0b00000010,
        0b00001100,
    // FULL
        0b00011111,
        0b00011111,
        0b00011111,
        0b00011111,
        0b00011111,
        0b00011111,
        0b00011111,
    // COLON
        0b00000000,
        0b00000110,
        0b00000110,
        0b00000000,
        0b00000110,
        0b00000110,
        0b00000000,
    // DASH
        0b00000000,
        0b00000000,
        0b00000000,
        0b00001110,
        0b00000000,
        0b00000000,
        0b00000000,
    // EMPTY
        0b00000000,
        0b00000000,
        0b00000000,
        0b00000000,
        0b00000000,
        0b00000000,
        0b00000000
};

class SDA5708 {
    uint8_t pinLoad;
    uint8_t pinData;
    uint8_t pinClock;

    void write(uint8_t value) {
        digitalWrite(pinLoad, LOW);

        for(int8_t pos = 0; pos < 8; ++pos) {
            digitalWrite(pinData, value & (1 << pos));

            digitalWrite(pinClock, HIGH);
            digitalWrite(pinClock, LOW);
        }

        digitalWrite(pinLoad, HIGH);
    }

    void selectDigit(uint8_t digit) {
        uint8_t initial = 0b10100000;
        write((digit & 0b111) | initial);
    }

public:
    SDA5708(uint8_t pinLoad, uint8_t pinData, uint8_t pinClock)
        : pinLoad(pinLoad), pinData(pinData), pinClock(pinClock) {
        pinMode(pinLoad, OUTPUT);
        pinMode(pinData, OUTPUT);
        pinMode(pinClock, OUTPUT);

        // Enable display
        write(0b11100000);
    }

    void writeChar(uint8_t column, char character) {
        selectDigit(column);

        // Use the 'full box' character as the default one
        uint8_t fontIndex = 10;
        if('0' <= character && character <= '9') {
            fontIndex = character - '0';
        } else {
            switch(character) {
                case ':': fontIndex = 11; break;
                case '-': fontIndex = 12; break;
                case ' ': fontIndex = 13; break;
            }
        }

        for(size_t row = 0; row < 7; ++row) {
            write(pgm_read_byte_near(DISPLAY_FONT + fontIndex * 7 + row));
        }
    }

    void writeTwoDigitNumber(uint8_t column, uint8_t number) {
        writeChar(column, '0' + number / 10);
        writeChar(column + 1, '0' + number % 10);
    }
};

